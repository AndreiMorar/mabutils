package com.mab.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Base64;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.content.pm.Signature;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Miscellaneous utils
 *
 * @author MAB
 */
public class UMisc {

    /**
     * Hides the soft keyboard
     *
     * @param context
     * @return
     */
    public static void hideSoftKeyboard(Context context) {
        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = ((Activity) context).getCurrentFocus();
        if (currentFocus != null) {
            inputManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * Hides the soft keyboard opened/focused on a specific edit text
     *
     * @param context
     * @return
     */
    public static void hideSoftKeyboard(Context context, EditText et) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
    }

    /**
     * Shows the soft keyboard
     *
     * @param context
     * @return
     */
    public static void showSoftKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    /**
     * @param objs
     * @return false if at least ONE object in the array is null OR if the entire list is null
     */
    public static boolean allObjectsExist(Object... objs) {

        if (objs == null) {
            return false;
        }

        for (Object pbj : objs) {
            if (pbj == null) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param ctx
     * @return the name of the current app running. May return "" if something goes wrong.
     */
    public static String getAppVersionName(Context ctx) {
        PackageInfo pInfo = null;
        try {
            pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return "";
    }

    /**
     * @param ctx
     * @return the code of the current app running. May return -1 if something goes wrong.
     */
    public static int getAppVersionCode(Context ctx) {
        PackageInfo pInfo = null;
        try {
            pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return -1;
    }

    /**
     * Return the SHA-1 which needs to be set in the Facebook Developer console
     *
     * @param ctx
     * @return the SHA-1 if successfull or "FAILED" if failed
     * @author MAB
     */
    public static final String getFacebookSHA1(Context ctx) {
        try {

            PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.DEFAULT).toString();
            }

        } catch (PackageManager.NameNotFoundException e) {
            return "FAILED";
        } catch (NoSuchAlgorithmException e) {
            return "FAILED";
        }

        return "FAILED";
    }

    /**
     * Obtain the URI of a resource
     *
     * @param context
     * @param resID
     * @return
     */
    public static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }

    /**
     * @param ctx
     * @param serviceClassName perform a .class.getName() on the class which extends Service
     * @return
     */
    public static boolean isServiceRunning(Context ctx, String serviceClassName) {
        if (ctx != null && serviceClassName != null) {
            ActivityManager manager = (ActivityManager) ctx.getSystemService(ctx.ACTIVITY_SERVICE);
            if (manager != null) {
                for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                    if (serviceClassName.equals(service.service.getClassName())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * These resources must be declared in the dimens.xml file as i.e.:<br/>
     * <b>&lt;item name="[...]" format="float" type="dimen"&gt;[...]&lt;/item&gt;</b>
     *
     * @param resources
     * @param resId
     * @return May return -1 if resources null
     */
    public static float getResourcesFloat(Resources resources, int resId) {
        if (resources == null) {
            return -1f;
        }
        TypedValue outValue = new TypedValue();
        resources.getValue(resId, outValue, true);

        return outValue.getFloat();
    }

    /**
     * Using the package name of this app, it checks to see if the app is currently running.
     * Mostly used from a service to see if the app still exists.
     *
     * @param ctx
     * @return
     */
    public static boolean isOurAppRunning(Context ctx) {
        List<ActivityManager.RunningAppProcessInfo> procInfos = ((ActivityManager) ctx.getSystemService(ctx.ACTIVITY_SERVICE)).getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals(ctx.getPackageName())) {
                return true;
            }
        }
        return false;
    }

}