package com.mab.utils;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import java.util.Random;

/**
 * @author MAB
 */
public class UViews {
    public static final int IGNORE = -351351366;

    private static final int STANDARD_ANIMATION_SPEED = 300;

    private static final float SLIDE_ANIMATION_OVERSHOOT_INTERPOLATOR_VALUE = 1.5f;

    /**
     * Assigns the same click listener to one ore more views at the same time
     *
     * @param listener
     * @param views
     */
    public static final void setOnClickListenerToViews(View.OnClickListener listener, View... views) {
        if (views != null && listener != null) {
            for (View view : views) {
                if (view != null) {
                    view.setOnClickListener(listener);
                }
            }
        }
    }

    /**
     * Searches a list of views in the content view (by their resource ID) and sets the same click listener to all of them
     *
     * @param listener
     * @param contentView the container on which we look for the views by their resource ID
     * @param viewResIds  the list of resource IDs of the views to which we need to attach the listener
     */
    public static final void setOnClickListenerToViews(View.OnClickListener listener, View contentView, int... viewResIds) {
        if (listener != null && contentView != null && viewResIds != null) {

            for (int id : viewResIds) {
                View view = contentView.findViewById(id);
                if (view != null) {
                    view.setOnClickListener(listener);
                }
            }

        }
    }

    /**
     * Assigns the same touch listener to one ore more views at the same time
     *
     * @param listener
     * @param views
     */
    public static final void setOnTouchListenerToViews(View.OnTouchListener listener, View... views) {
        if (views != null && listener != null) {
            for (View view : views) {
                if (view != null) {
                    view.setOnTouchListener(listener);
                }
            }
        }
    }


    /**
     * Set a string to a textview. It checks for null values on objects.
     *
     * @param view
     * @param text
     * @param defaultValue
     * @return true if it managed to set the string
     * @author MAB
     */
    public static boolean setTextToView(TextView view, String text, String defaultValue) {
        if (view != null) {
            if (text != null) {
                view.setText(text);
                return true;
            } else {
                view.setText(defaultValue);
                return true;
            }
        }

        return false;
    }

    /**
     * Set a string to a textview. It checks for null values on objects.
     *
     * @param view
     * @param text
     * @return true if it managed to set the string
     * @author MAB
     */
    public static boolean setTextToView(TextView view, String text) {
        return UViews.setTextToView(view, text, "");
    }

    /**
     * @param views
     * @return false if at least ONE view in the array is null OR if the entire list is null
     */
    public static boolean allViewsExist(View... views) {

        if (views == null) {
            return false;
        }

        for (View view : views) {
            if (view == null) {
                return false;
            }
        }

        return true;
    }

    /**
     * In some cases, the onDraw method is not called on a view. This method enables that.
     *
     * @param view
     */
    public static final void enableOnDrawCalls(View view) {

        if (view == null) {
            return;
        }

        // IMPORTANT: if this is not set to false, it won't call draw/onDraw
        view.setWillNotDraw(false);

        // IMPORTANT: for 4.0 and above
        view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    /**
     * Set the visibility of all the views to View.GONE
     *
     * @param views
     * @author MAB
     */
    public static final void makeGone(View... views) {
        if (views == null) {
            return;
        }
        for (View view : views) {
            if (view != null) {
                view.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Set the visibility of all the views to View.INVISIBLE
     *
     * @param views
     * @author MAB
     */
    public static final void makeInvisible(View... views) {
        if (views == null) {
            return;
        }
        for (View view : views) {
            if (view != null) {
                view.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Set the visibility of all the views to View.VISIBLE
     *
     * @param views
     * @author MAB
     */
    public static final void makeVisible(View... views) {
        if (views == null) {
            return;
        }
        for (View view : views) {
            if (view != null) {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Set the visibility (visible or invisible) to one or more views
     *
     * @param visible if true it will set the visibility to View.VISIBLE, otherwise View.INVISIBLE
     * @param views
     */
    public static final void setVisibility(boolean visible, View... views) {
        if (views == null) {
            return;
        }
        for (View view : views) {
            if (view != null) {
                view.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
            }
        }
    }

    /**
     * Set the visibility (visible or invisible) to the specified view
     *
     * @param visible if true it will set the visibility to View.VISIBLE, otherwise View.INVISIBLE
     * @param view
     */
    public static final void setVisibility(boolean visible, View view) {
        if (view == null) {
            return;
        }
        view.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    /**
     * Add a view to a viewgroup
     *
     * @param what  the view to be added to the viewgroup
     * @param where the viewgroup in which we want to add the view
     * @return true if it added it correctly (i.e. none of the views were null)
     */
    public static boolean addView(View what, ViewGroup where) {
        if (what != null && where != null) {
            where.addView(what);
            return true;
        }

        return false;
    }

    /**
     * IMPORTANT !!! Call this only after you're sure the view has been drawn, i.e. it's dimensions are already calcualted
     *
     * @return May return an empty object if the view is null or it's not yet been measured
     */
    public static Vector2 getDimensions(View view) {
        if (view == null) {
            return Vector2.instance();
        }

        return Vector2.instanceDimen(view.getWidth(), view.getHeight());
    }

    /**
     * @param view
     * @param colorValue a color value, not a resource ID !!!
     * @param shapeResId the ID of the resource which refers to a shape NOT inside a selector
     */
    public static void setColorToSelectorNormalState(View view, int shapeResId, int colorValue) {
        if (view != null) {

            GradientDrawable shapeDrawable = (GradientDrawable) view.getResources().getDrawable(shapeResId);
            shapeDrawable.setColor(colorValue);

            StateListDrawable states = new StateListDrawable();
            states.addState(new int[]{}, shapeDrawable);

            setBackgroundDrawable(view, states);

        }
    }

    /**
     * Sets a drawable as a background to a view but checks for API compatibility.
     * Before API 16 we used setBackgroundDrawable(), after it we use setBackground().
     *
     * @param view
     * @param drawable
     */
    @SuppressLint("NewApi")
    public static void setBackgroundDrawable(View view, Drawable drawable) {
        if (view != null) {
            int sdk = Build.VERSION.SDK_INT;
            if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
                view.setBackgroundDrawable(drawable);
            } else {
                view.setBackground(drawable);
            }
        }
    }

    /**
     * Calls the listener when the onPreDraw method of the {@link android.view.ViewTreeObserver.OnPreDrawListener} is called.
     * This method will handle it so the onPreDraw is only called once and will remove the listener.
     *
     * @param view
     * @param listener
     */
    public static void waitForOnPreDraw(final View view, final IOnPreDawListener listener) {
        if (view == null) {
            return;
        }
        ViewTreeObserver treeObs = view.getViewTreeObserver();
        if (treeObs == null) {
            if (listener != null) {
                listener.onPreDraw(view);
            }
            return;
        }
        treeObs.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (view == null) {
                    if (listener != null) {
                        listener.onPreDraw(null);
                    }
                    return true;
                }
                // IMPORTANT !!! If not removed, it will enter an infinite loop
                ViewTreeObserver treeObs = view.getViewTreeObserver();
                if (treeObs != null && treeObs.isAlive()) {
                    treeObs.removeOnPreDrawListener(this);
                }
                if (listener != null) {
                    listener.onPreDraw(view);
                }
                return true;
            }
        });

    }

    /**
     * Calls the listener when the onGlobalLayout method of the {@link android.view.ViewTreeObserver.OnGlobalLayoutListener} is called.
     * This method will handle it so the onGlobalLayout is only called once and will remove the listener.
     *
     * @param view
     * @param listener
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void waitForOnGlobalLayout(final View view, final IOnGlobalLayoutListener listener) {
        if (view == null) {
            return;
        }
        ViewTreeObserver treeObs = view.getViewTreeObserver();
        if (treeObs == null) {
            if (listener != null) {
                listener.onGlobalLayout(view);
            }
            return;
        }
        treeObs.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (view == null) {
                    if (listener != null) {
                        listener.onGlobalLayout(null);
                    }
                    return;
                }
                // IMPORTANT !!! If not removed, it will enter an infinite loop
                ViewTreeObserver treeObs = view.getViewTreeObserver();
                if (treeObs != null && treeObs.isAlive()) {
                    treeObs.removeOnGlobalLayoutListener(this);
                }
                if (listener != null) {
                    listener.onGlobalLayout(view);
                }
            }
        });
    }

    /**
     * @param view
     * @param dim  contains the dimensions that are required to be added to the view. This method will extract only the ones which have been populated, it will ignore the rest.
     */
    public static final void setDimensions(View view, Dimensions dim) {
        if (view == null || dim == null) {
            return;
        }

        view.setPadding(
                dim.getLeftPadding(view.getPaddingLeft()),
                dim.getTopPadding(view.getPaddingTop()),
                dim.getRightPadding(view.getPaddingRight()),
                dim.getBottomPadding(view.getPaddingBottom()));

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(
                dim.getLeftMargin(layoutParams.leftMargin),
                dim.getTopMargin(layoutParams.topMargin),
                dim.getRightMargin(layoutParams.rightMargin),
                dim.getBottomMargin(layoutParams.bottomMargin));

        if (dim.isHeightAvailable()) layoutParams.height = dim.getHeight();

        if (dim.isWidthAvailable()) layoutParams.width = dim.getWidth();

        view.requestLayout();

    }

    /**
     * Adds the same dimension object to multiple views
     *
     * @param dim   contains the dimensions that are required to be added to the view. This method will extract only the ones which have been populated, it will ignore the rest.
     * @param views
     */
    public static final void setDimensions(Dimensions dim, View... views) {
        if (views == null || views.length == 0 || dim == null) {
            return;
        }

        for (View view : views) {
            setDimensions(view, dim);
        }

    }

    /**
     * Enable/disable the clickable function on one or more views
     *
     * @param isClickable
     * @param views
     */
    public static final void setClickable(boolean isClickable, View... views) {
        if (views == null || views.length == 0) {
            return;
        }

        for (View view : views) {
            if (view != null) {
                view.setClickable(isClickable);
            }
        }

    }

    /**
     * Creates a sliding animation from a views position to a given X and Y coordinates.
     *
     * @param view           The view we apply the animation to.
     * @param slideToX       The destination's X coordinate.
     * @param slideToY       The destination's Y coordinate.
     * @param animationSpeed The duration of the animation. The longer it lasts the slower it gets.
     */
    public static void createSlideAnimation(View view, float slideToX, float slideToY, int animationSpeed, Animator.AnimatorListener animatorListener) {
        Random random = new Random();

        view.animate()
                .setDuration(animationSpeed)
                .setInterpolator(new OvershootInterpolator(SLIDE_ANIMATION_OVERSHOOT_INTERPOLATOR_VALUE))
                .x(slideToX)
                .y(slideToY)
                .setStartDelay(random.nextInt(100))
                .setListener(animatorListener);
    }

    /**
     * Creates a sliding animation from a views position to a given X and Y coordinates.
     *
     * @param view     The view we apply the animation to.
     * @param slideToX The destination's X coordinate.
     * @param slideToY The destination's Y coordinate.
     */
    public static void createSlideAnimation(View view, float slideToX, float slideToY, Animator.AnimatorListener animatorListener) {
        createSlideAnimation(view, slideToX, slideToY, STANDARD_ANIMATION_SPEED, animatorListener);
    }

    public static void setViewEnabledState(boolean enabled, View... views) {
        if (views == null || views.length == 0) {
            return;
        }
        for (View view : views) {
            if (view != null) {
                view.setEnabled(enabled);
            }
        }
    }

    //------------------------------------------------- INTERFACE(start)
    public static interface IOnPreDawListener {
        /**
         * This runs on the caller's thread
         *
         * @param view
         */
        void onPreDraw(View view);
    }

    public static interface IOnGlobalLayoutListener {
        /**
         * This runs on the caller's thread
         *
         * @param view
         */
        void onGlobalLayout(View view);
    }
    //------------------------------------------------- INTERFACE(end)

}