package com.mab.utils;

import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

public class UGson {

    private static UGson instance;

    private Gson gson = null;

    private UGson() {
        gson = new Gson();
    }

    public static final <T> T fromJson(String json, Type type) {
        if (instance == null) {
            instance = new UGson();
        }
        return instance.gson.fromJson(json, type);
    }

    public static <T> T jsonToObject(InputStream databaseInputStream, Class<T> toClass) throws Exception {
        if (instance == null) {
            instance = new UGson();
        }
        return instance.gson.fromJson(new InputStreamReader(databaseInputStream), toClass);
    }

    public static <T> T jsonToObject(File file, Class<T> toClass) throws Exception {
        if (instance == null) {
            instance = new UGson();
        }
        return instance.gson.fromJson(new InputStreamReader(new FileInputStream(file)), toClass);
    }

    public static <T> T jsonToObject(byte[] byteArray, Class<T> toClass, String charset) throws Exception {
        if (instance == null) {
            instance = new UGson();
        }
        return instance.gson.fromJson(new InputStreamReader(new ByteArrayInputStream(byteArray), charset), toClass);
    }

    public static <T> String objectToJson(Object o, Class<T> toClass) throws Exception {
        if (instance == null) {
            instance = new UGson();
        }
        return instance.gson.toJson(o, toClass);
    }

    public static <T> String objectToJson(Object o) throws Exception {
        if (instance == null) {
            instance = new UGson();
        }
        return instance.gson.toJson(o);
    }
}