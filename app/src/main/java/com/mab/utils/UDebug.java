package com.mab.utils;

import android.support.v4.BuildConfig;
import android.util.Log;

import javax.security.auth.x500.X500Principal;

/**
 * @author MAB
 */
public class UDebug {

    /** Used by the method that checks if the app is in debug mode or not */
    private static final X500Principal DEBUG_DN = new X500Principal("CN=Android Debug,O=Android,C=US");

    /**
     * Check if the app is in debug mode or not.<br/>
     * According to the info in Android documentation Signing Your Application,
     * debug key contain following subject distinguished name: "CN=Android Debug,O=Android,C=US".
     * We can use this information to test if login is signed with debug key without hardcoding
     * debug key signature into our code.
     *
     * @return
     */
    public static boolean isDebuggable() {
        return (BuildConfig.DEBUG);
    }

    /**
     * Will throw an @IllegalArgumentException
     *
     * @param str
     */
    public static final void forceCrash(String str) {
        if (true) {
            throw new IllegalArgumentException(str);
        }
    }

    /**
     * Will throw an @IllegalArgumentException
     */
    public static final void forceCrash() {
        forceCrash("Test crash");
    }

    /**
     * Returns the stack trace. Used for debugging.
     *
     * @return
     */
    public static String getStackTrace() {
        return Log.getStackTraceString(new Exception());
    }

    /**
     * Sysout's the stack trace. Used for debugging.
     *
     * @return
     */
    public static void printStackTrace() {
        System.out.println(Log.getStackTraceString(new Exception()));
    }

    public static void printExceptionStackTrace(Throwable e) {
        if (e != null) {
            if (isDebuggable()) {
                System.out.println("============= STACK TRACE (start)");
                e.printStackTrace();
                System.out.println("============= STACK TRACE (end)");
            }
        }

    }

}