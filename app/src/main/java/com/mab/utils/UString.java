package com.mab.utils;

import android.text.TextUtils;

import java.util.Currency;

/**
 * @author MAB
 */
public class UString {

    /**
     * @return false if the string is either null or empty<br/>
     * true otherwise
     * @author MAB
     */
    public static boolean stringExists(String str) {
        return (str != null && !str.isEmpty());
    }

    /**
     * @return false if one of the strings in the array is either null or empty<br/>
     * true otherwise
     * @author MAB
     */
    public static boolean stringsExist(String... values) {

        for (String str : values) {
            if (str == null || str.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Compares two strings but checks for null values. <br/>
     * <b>IMPORTANT</b> !!!<br/>
     * 1. The check is not case sensitive<br/>
     * 2. If both of the values are null, then it will still return true
     *
     * @param str1
     * @param str2
     * @return
     */
    public static boolean stringsEqual(String str1, String str2) {
        //If both are null, then we're ok
        if (str1 == null && str2 == null) {
            return true;
        }

        //If just one is null, then stopPlaying
        if (str1 == null || str2 == null) {
            return false;
        }

        return str1.compareToIgnoreCase(str2) == 0;

    }

    /**
     * @param str
     * @return "" in case the calling param is null
     */
    public static String getString(String str) {
        return str == null ? "" : str;
    }

    /**
     * @param str
     * @param defaultStr
     * @return @defaultString in case the calling param is null
     */
    public static String getString(String str, String defaultStr) {
        return str == null ? defaultStr : str;
    }

    /**
     * @param isoCode three letter word representing the ISO currency code
     * @return if the code is not recognized, it will just echo the entered #isoCode
     */
    public static String getCurrencySymbol(String isoCode) {
        if (isoCode == null) {
            return isoCode;
        }
        try {
            return Currency.getInstance(isoCode).getSymbol();
        } catch (IllegalArgumentException e) {
            return isoCode;
        }
    }

    /**
     * @param target CharSequence - email address provided by the user
     * @return true if the email address is valid otherwise false
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}