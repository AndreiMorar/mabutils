package com.mab.utils;

/**
 * @author MAB
 */
public class Trifecta<F, S, T> {
    public final F first;
    public final S second;
    public final T third;

    public Trifecta(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public static <A, B, C> Trifecta<A, B, C> create(A a, B b, C c) {
        return new Trifecta(a, b, c);
    }

    /**
     * @return true if all values are not null
     */
    public boolean allExist() {
        return (first != null && second != null && third != null);
    }

    /**
     * @return true if the first object is null
     */
    public boolean firstNull() {
        return first == null;
    }

    /**
     * @return true if the second object is null
     */
    public boolean secondNull() {
        return second == null;
    }

    /**
     * @return true if the third object is null
     */
    public boolean thirdNull() {
        return third == null;
    }
}