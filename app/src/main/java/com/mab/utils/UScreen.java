package com.mab.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * @author MAB
 */
public class UScreen {

    /**
     * Determines the width and height of the screen in the current orientation.<br/>
     * This method will return switched values when orientation changes so recall it if necessary.
     *
     * @param context
     * @return int[2] array with first element being the width of the screen in the current orientation and the second being the height of the screen in the current orientation<br/>
     * int[0] - width<br/>
     * int[1] - height<br/>
     * Values is in <b>pixels</b>
     */
    public static int[] getScreenDimensions(Context context) {
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        return (new int[]{display.widthPixels, display.heightPixels});
    }

    /**
     * Determines the width of the screen in the current orientation.<br/>
     * This method will return switched values when orientation changes so recall it if necessary.
     *
     * @param context
     * @return the width in pixels
     * @author MAB
     */
    public static int getScreenWidth(Context context) {
        return UScreen.getScreenDimensions(context)[0];
    }

    /**
     * Determines the height of the screen in the current orientation.<br/>
     * This method will return switched values when orientation changes so recall it if necessary.
     *
     * @param context
     * @return the height in pixels
     * @author MAB
     */
    public static int getScreenHeight(Context context) {
        return UScreen.getScreenDimensions(context)[1];
    }


    /**
     * Determines the width of the screen always in portrait mode (i.e. it takes the smallest value).<br/>
     *
     * @param context
     * @return the width in pixels
     * @author MAB
     */
    public static int getScreenWidthInPortrait(Context context) {
        int[] dimen = getScreenDimensions(context);
        if (dimen[0] < dimen[1]) {
            return dimen[0];
        } else {
            return dimen[1];
        }
    }

    /**
     * Determines the aspect ratio of the screen in the current orientation.<b>(width / height)</b><br/>
     * This method will return switched values when orientation changes so recall it if necessary.
     *
     * @return
     */
    public static float getScreenAspectRatio(Context context) {
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        return (float) (((float) display.widthPixels / (float) display.heightPixels));
    }

    /**
     * Determines if the current device is in portrait mode or not
     *
     * @param context
     * @return <b>true</b> if in {@link android.content.res.Configuration#ORIENTATION_PORTRAIT}<br/>
     * <b>false</b> for any other case, i.e. {@link android.content.res.Configuration#ORIENTATION_LANDSCAPE}, {@link android.content.res.Configuration#ORIENTATION_SQUARE} and {@link android.content.res.Configuration#ORIENTATION_UNDEFINED}
     * @author MAB
     */
    public static boolean isInPortraitMode(Context context) {
        return (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into d
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    /**
     * This method converts sp unit to equivalent pixels, depending on device scaled density.
     *
     * @param sp      A value in sp unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to sp depending on device density
     */
    public static float convertSpToPixel(float sp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = sp * metrics.scaledDensity;
        return px;
    }

    /**
     * This method converts device specific pixels to sp.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into sp
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent sp equivalent to px value
     */
    public static float convertPixelsToSp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float sp = px / metrics.scaledDensity;
        return sp;
    }


    /**
     * Calculates the left | top | right | bottom  position of a view RELATIVE TO THE SCREEN (to the root parent)<br/>
     * This method is recursive. It goes up the "view tree" and it keeps adding the #side value for each one<br/><br/>
     * <p/>
     * <b>Note: </b> This does not adjust the sum for negative values (in case the view is out of screen bounds).<br/>
     * If negative values will occur, they will just get added to the rest of the offset values (in fact they will get subtracted from the total sum)<br/><br/>
     * <p/>
     * <b>IMPORTANT !!!!</b> Always call this method when you're sure the view and parent views have been drawn and ave known margins
     *
     * @param myView     the view for which we need to determine the offset
     * @param offsetType Possible values: [0 - left], [1 - top], [2 - right], [3- bottom]
     * @return 0 in case  the view is null<br/>
     * A value <= 0 in case there are one or more offsets < 0 and the positive values are < than the negative ones<br/>
     * A value >= 0 if everything is ok
     */
    public static int getPositionRelativeToRootView(View myView, int offsetType) {

        if (myView == null) {
            return 0;
        }

        /*
         * Obtain the offset position of this current view
         */
        int offset = 0;
        switch (offsetType) {
            //LEFT
            case 0:
                offset = myView.getLeft();
                break;
            //TOP
            case 2:
                offset = myView.getTop();
                break;
            //RIGHT
            case 3:
                offset = myView.getRight();
                break;
            //BOTTOM
            case 4:
                offset = myView.getBottom();
                break;
        }

        //If this view is the root, then return the determined value
        if (myView.getParent() == myView.getRootView()) {
            return offset;
        } else { // Otherwise, add the offset of our parent (recursive portion)
            return offset + getPositionRelativeToRootView((View) myView.getParent(), offsetType);
        }
    }

    /**
     * Removes the description bar at the top of the screen (this is the one with the app's icon on it)
     *
     * @param activity
     */
    public static void removeTitleBar(Activity activity) {
        if (activity != null) {
            activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        }

    }

    /**
     * Removes the notification bar at the top of the screen (this is the one with the time display, battery icon, etc)
     *
     * @param activity
     */
    public static void removeNotificationBar(Activity activity) {
        if (activity != null) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    /**
     * Does not let the screen go into standby mode
     *
     * @param activity
     */
    public static void keepScreenAlwaysOn(Activity activity) {
        if (activity != null) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public static boolean isScreenSmall(Resources res) {
        return ((res.getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL);
    }

    public static boolean isScreenNormal(Resources res) {
        return ((res.getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL);
    }

    public static boolean isScreenLarge(Resources res) {
        return ((res.getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
    }

    public static boolean isScreenXLarge(Resources res) {
        return ((res.getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
    }

    /**
     * Returns true if the current device has a small or normal screen
     *
     * @param res
     * @return
     */
    public static boolean isPhone(Resources res) {
        return isScreenSmall(res) || isScreenNormal(res);
    }

    /**
     * Returns true if the current device has neither a small nor a normal screen
     *
     * @param res
     * @return
     */
    public static boolean isTablet(Resources res) {
        return !isScreenSmall(res) && !isScreenNormal(res);
    }


    /**
     * @param ctx
     * @param percentage must be between 0-100
     * @return
     */
    public static int getWidthPercentageInPixel(Context ctx, float percentage) {
        return (int) (getScreenWidth(ctx) * percentage / 100f);
    }

    /**
     * @param ctx
     * @param percentage must be between 0-100
     * @return
     */
    public static int getHeightPercentageInPixel(Context ctx, float percentage) {
        return (int) (getScreenHeight(ctx) * percentage / 100f);
    }

}