package com.mab.utils;

/**
 * Used for quick storage of multiple dimension values.
 *
 * @author MAB
 */
public class Dimensions {

    private int DEFAULT = -351135135;

    private int leftMargin = DEFAULT;
    private int topMargin = DEFAULT;
    private int rightMargin = DEFAULT;
    private int bottomMargin = DEFAULT;
    private int width = DEFAULT;
    private int height = DEFAULT;
    private int leftPadding = DEFAULT;
    private int topPadding = DEFAULT;
    private int rightPadding = DEFAULT;
    private int bottomPadding = DEFAULT;

    //=================================================================
    //=================================================================
    //================================================================= ONLY SETTERS BELOW !!!!!!!!!!
    //=================================================================
    //=================================================================

    /**
     * SETTER
     *
     * @param margin left margin
     * @return
     */
    public Dimensions leftMargin(int margin) {
        leftMargin = margin;
        return this;
    }

    /**
     * SETTER
     *
     * @param margin right margin
     * @return
     */
    public Dimensions rightMargin(int margin) {
        rightMargin = margin;
        return this;
    }

    /**
     * SETTER
     *
     * @param margin top margin
     * @return
     */
    public Dimensions topMargin(int margin) {
        topMargin = margin;
        return this;
    }

    /**
     * SETTER
     *
     * @param margin top margin
     * @return
     */
    public Dimensions bottomMargin(int margin) {
        bottomMargin = margin;
        return this;
    }

    /**
     * Sets the same value for both LEFT and RIGHT margins
     *
     * @param margin
     * @return
     */
    public Dimensions horizontalMargins(int margin) {
        leftMargin = margin;
        rightMargin = margin;
        return this;
    }

    /**
     * Sets the same value for both TOP and BOTTOM margins
     *
     * @param margin
     * @return
     */
    public Dimensions verticalMargins(int margin) {
        topMargin = margin;
        bottomMargin = margin;
        return this;
    }

    /**
     * Set the same value for all the margins
     *
     * @param margin
     * @return
     */
    public Dimensions margins(int margin) {
        leftMargin = margin;
        topMargin = margin;
        rightMargin = margin;
        bottomMargin = margin;
        return this;
    }

    /**
     * SETTER
     *
     * @param padding left padding
     * @return
     */
    public Dimensions leftPadding(int padding) {
        leftPadding = padding;
        return this;
    }

    /**
     * SETTER
     *
     * @param padding right padding
     * @return
     */
    public Dimensions rightPadding(int padding) {
        rightPadding = padding;
        return this;
    }

    /**
     * SETTER
     *
     * @param padding top padding
     * @return
     */
    public Dimensions topPadding(int padding) {
        topPadding = padding;
        return this;
    }

    /**
     * SETTER
     *
     * @param padding top padding
     * @return
     */
    public Dimensions bottomPadding(int padding) {
        bottomPadding = padding;
        return this;
    }

    /**
     * Sets the same value for both LEFT and RIGHT paddings
     *
     * @param padding
     * @return
     */
    public Dimensions horizontalPaddings(int padding) {
        leftPadding = padding;
        rightPadding = padding;
        return this;
    }

    /**
     * Sets the same value for both TOP and BOTTOM paddings
     *
     * @param padding
     * @return
     */
    public Dimensions verticalPaddings(int padding) {
        topPadding = padding;
        bottomPadding = padding;
        return this;
    }

    /**
     * Set the same value for all the paddings
     *
     * @param padding
     * @return
     */
    public Dimensions paddings(int padding) {
        leftPadding = padding;
        topPadding = padding;
        rightPadding = padding;
        bottomPadding = padding;
        return this;
    }


    public Dimensions width(int width) {
        this.width = width;
        return this;
    }

    public Dimensions height(int height) {
        this.height = height;
        return this;
    }

    /**
     * Set the same value for both the WIDTH and HEIGHT
     *
     * @param size
     * @return
     */
    public Dimensions square(int size) {
        width = size;
        height = size;
        return this;
    }

    //=================================================================
    //=================================================================
    //================================================================= ONLY GETTERS BELOW !!!!!!!!!!
    //=================================================================
    //=================================================================

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getLeftMargin(int defaultValue) {
        return leftMargin == DEFAULT ? defaultValue : leftMargin;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getRightMargin(int defaultValue) {
        return rightMargin == DEFAULT ? defaultValue : rightMargin;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getTopMargin(int defaultValue) {
        return topMargin == DEFAULT ? defaultValue : topMargin;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getBottomMargin(int defaultValue) {
        return bottomMargin == DEFAULT ? defaultValue : bottomMargin;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getLeftPadding(int defaultValue) {
        return leftPadding == DEFAULT ? defaultValue : leftPadding;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getRightPadding(int defaultValue) {
        return rightPadding == DEFAULT ? defaultValue : rightPadding;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getTopPadding(int defaultValue) {
        return topPadding == DEFAULT ? defaultValue : topPadding;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getBottomPadding(int defaultValue) {
        return bottomPadding == DEFAULT ? defaultValue : bottomPadding;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getWidth(int defaultValue) {
        return width == DEFAULT ? defaultValue : width;
    }

    public int getWidth() {
        return width;
    }

    /**
     * @param defaultValue in case the existing value is not available, it will echo the defaultValue
     * @return
     */
    public int getHeight(int defaultValue) {
        return height == DEFAULT ? defaultValue : height;
    }

    public int getHeight() {
        return height;
    }

    public boolean isWidthAvailable() {
        return width != DEFAULT;
    }

    public boolean isHeightAvailable() {
        return height != DEFAULT;
    }

    @Override
    public String toString() {
        return "Dimensions{" +
                ", \nleftMargin=" + leftMargin +
                ", \ntopMargin=" + topMargin +
                ", \nrightMargin=" + rightMargin +
                ", \nbottomMargin=" + bottomMargin +
                ", \nwidth=" + width +
                ", \nheight=" + height +
                ", \nleftPadding=" + leftPadding +
                ", \ntopPadding=" + topPadding +
                ", \nrightPadding=" + rightPadding +
                ", \nbottomPadding=" + bottomPadding +
                '}';
    }
}