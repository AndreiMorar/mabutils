package com.mab.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

/**
 * @author MAB
 */
public class Vector2 {

    /**
     * Coordinates (int value)<br/>
     * Default value -1
     */
    public int nX = -1, nY = -1;

    /**
     * Coordinates (float value)<br/>
     * Default value -1
     */
    public float fX = -1, fY = -1;

    /**
     * Positions (int value)<br/>
     * Default value -1
     */
    public int nTop = -1, nBottom = -1;

    /**
     * Positions (float value)<br/>
     * Default value -1
     */
    public float fTop = -1, fBottom = -1;

    /**
     * Dimensions (int value)<br/>
     * Default value -1
     */
    public int nWidth = -1, nHeight = -1;

    /**
     * Dimensions (float value)<br/>
     * Default value -1
     */
    public float fWidth = -1, fHeight = -1;

    private Vector2() {
    }

    @Override
    public String toString() {
        String str = "";
        str += (fWidth == -1f && fHeight == -1f) ? "" : "[dimen = " + fWidth + " x " + fHeight + "]";
        str += (fX == -1f && fX == -1f) ? "" : "[pos = " + fX + " x " + fY + "]";
        str += (fTop == -1f && fBottom == -1f) ? "" : "[top/bottom = " + fTop + " x " + fBottom + "]";

        if (str.equals("")) {
            str = "[dimen = " + fWidth + " x " + fHeight + "]" + "[pos = " + fX + " x " + fY + "]" + "[top/bottom = " + fTop + " x " + fBottom + "]";
        }

        return str;
    }

    /**
     * Instantiate a new Vector2
     *
     * @return
     */
    public static Vector2 instance() {
        return new Vector2();
    }

    /**
     * Instantiate a new Vector2 and at the same time setup the coord values
     *
     * @return
     */
    public static Vector2 instanceCoord(int x, int y) {
        Vector2 v2 = new Vector2();
        v2.coord(x, y);
        return v2;
    }

    /**
     * Instantiate a new Vector2 and at the same time setup the coord values
     *
     * @return
     */
    public static Vector2 instanceCoord(float x, float y) {
        Vector2 v2 = new Vector2();
        v2.coord(x, y);
        return v2;
    }

    /**
     * Instantiate a new Vector2 and at the same time setup the coord values, vales being taken from the motion event
     *
     * @return
     */
    public static Vector2 instanceCoordFromTouchEvent(MotionEvent event) {
        Vector2 v2 = new Vector2();
        if (event != null) {
            v2.coord(event.getX(), event.getY());
        }
        return v2;
    }

    /**
     * Instantiate a new Vector2 and at the same time setup the position values
     *
     * @return
     */
    public static Vector2 instancePosition(float top, float bottom) {
        Vector2 v2 = new Vector2();
        v2.position(top, bottom);
        return v2;
    }

    /**
     * Instantiate a new Vector2 and at the same time setup the position values
     *
     * @return
     */
    public static Vector2 instancePosition(int top, int bottom) {
        Vector2 v2 = new Vector2();
        v2.position(top, bottom);
        return v2;
    }

    /**
     * Instantiate a new Vector2 and at the same time setup the dimension values
     *
     * @return
     */
    public static Vector2 instanceDimen(int width, int height) {
        Vector2 v2 = new Vector2();
        v2.dimen(width, height);
        return v2;
    }

    /**
     * Instantiate a new Vector2 and at the same time setup the dimension values
     *
     * @return
     */
    public static Vector2 instanceDimen(float width, float height) {
        Vector2 v2 = new Vector2();
        v2.dimen(width, height);
        return v2;
    }

    /**
     * Instantiates a new Vector2 and populates the dimen values with the dimensions of the screen<br/>
     * Note !! The screen dimensions are dependant on the current orientation of the device. If the orientation changes, this same call will return swapped values.
     *
     * @return
     */
    public static Vector2 instanceScreenDim(Context ctx) {
        DisplayMetrics display = ctx.getResources().getDisplayMetrics();

        Vector2 v2 = new Vector2();
        v2.dimen(display.widthPixels, display.heightPixels);

        return v2;
    }

    /**
     * Creates a new instance with the same values as the ones inside the request paramt
     *
     * @param vect
     * @return May return null in case the request param was null
     */
    public static Vector2 copyByValue(@Nullable Vector2 vect) {
        if (vect == null) {
            return null;
        }

        Vector2 newVect = new Vector2();

        newVect.dimen(vect.fWidth, vect.fHeight);
        newVect.coord(vect.fX, vect.fY);
        newVect.position(vect.nTop, vect.nBottom);

        return newVect;
    }

    /**
     * Set width and height dimensions
     *
     * @param width
     * @param height
     */
    public Vector2 dimen(int width, int height) {
        nWidth = width;
        nHeight = height;

        fWidth = (float) width;
        fHeight = (float) height;

        return this;
    }

    /**
     * Set width and height dimensions
     *
     * @param width
     * @param height
     */
    public Vector2 dimen(float width, float height) {
        fWidth = width;
        fHeight = height;

        nWidth = (int) width;
        nHeight = (int) height;

        return this;
    }

    /**
     * Set x and y coordinates
     *
     * @param x
     * @param y
     */
    public Vector2 coord(int x, int y) {
        nX = x;
        nY = y;

        fX = (float) x;
        fY = (float) y;

        return this;
    }

    /**
     * Set x and y coordinates
     *
     * @param x
     * @param y
     */
    public Vector2 coord(float x, float y) {
        fX = x;
        fY = y;

        nX = (int) x;
        nY = (int) y;

        return this;
    }

    /**
     * Set top and bottom positions (coordinates)
     *
     * @param top
     * @param bottom
     */
    public Vector2 position(int top, int bottom) {
        fTop = top;
        fBottom = bottom;

        nTop = (int) top;
        nBottom = (int) bottom;

        return this;
    }

    /**
     * Get the X and Y coordinates from the motion event
     *
     * @param event
     * @return
     */
    public Vector2 coordFromTouchEvent(MotionEvent event) {
        if (event != null) {
            coord(event.getX(), event.getY());
        }
        return this;
    }

    /**
     * Set top and bottom positions (coordinates)
     *
     * @param top
     * @param bottom
     */
    public Vector2 position(float top, float bottom) {
        fTop = top;
        fBottom = bottom;

        nTop = (int) top;
        nBottom = (int) bottom;

        return this;
    }

    /**
     * Check to see if the coordinates equal in value (int values)
     *
     * @param x
     * @param y
     * @return
     */
    public boolean equalCoords(int x, int y) {
        return (nX == x && nY == y);
    }

    /**
     * Check to see if the coordinates equal in value (float values)
     *
     * @param x
     * @param y
     * @return
     */
    public boolean equalCoords(float x, float y) {
        return (fX == x && fY == y);
    }

    /**
     * Check to see if the coordinates equal in value
     *
     * @param coords
     * @return
     */
    public boolean equalCoords(Vector2 coords) {
        if (coords == null) {
            return false;
        }
        return (nX == coords.nX && nY == coords.nY && fX == coords.fX && fY == coords.fY);
    }

    /**
     * Check to see if the positions equal in value (int values)
     *
     * @param top
     * @param bottom
     * @return
     */
    public boolean equalPositions(int top, int bottom) {
        return (nTop == top && nBottom == bottom);
    }

    /**
     * Check to see if the positions equal in value (float values)
     *
     * @param top
     * @param bottom
     * @return
     */
    public boolean equalPositions(float top, float bottom) {
        return (fTop == top && fBottom == bottom);
    }

    /**
     * Check to see if the positions equal in value
     *
     * @param positions
     * @return
     */
    public boolean equalPositions(Vector2 positions) {
        if (positions == null) {
            return false;
        }
        return (nTop == positions.nTop && nBottom == positions.nBottom && fTop == positions.fTop && fBottom == positions.fBottom);
    }

    /**
     * Check to see if the dimensions equal in value (int values)
     *
     * @param width
     * @param height
     * @return
     */
    public boolean equalDimens(int width, int height) {
        return (nWidth == width && nHeight == height);
    }

    /**
     * Check to see if the coordinates equal in value (float values)
     *
     * @param width
     * @param height
     * @return
     */
    public boolean equalDimens(float width, float height) {
        return (fWidth == width && fHeight == height);
    }

    /**
     * Check to see if the coordinates equal in value (float values)
     *
     * @param dimens
     * @return
     */
    public boolean equalDimens(Vector2 dimens) {
        return (nWidth == dimens.nWidth && nHeight == dimens.nHeight && fWidth == dimens.fWidth && fHeight == dimens.fHeight);
    }

    /**
     * @return the <b>width / height</b> result.<br/>
     */
    public float getAspectRatio() {
        return fWidth / nHeight;
    }

    /**
     * Swap the values for the width and height
     */
    public void swapDimensions() {
        dimen(fHeight, fWidth);
    }

    /**
     * Swap the values for the X and Y coordinates
     */
    public void swapCoordinates() {
        dimen(fY, fX);
    }

    /**
     * @return true if both coordinates are != -1 then
     */
    public boolean coordinatesExist() {
        return (fX != -1f && fY != -1f);
    }

    /**
     * @return true if both dimension values are != -1 then
     */
    public boolean dimensionsExist() {
        return (fWidth != -1f && fHeight != -1f);
    }

    /**
     * @return true if both position values are != -1 then
     */
    public boolean positionsExist() {
        return (fTop != -1f && fBottom != -1f);
    }


    /**
     * Creates a new instance with the same values as the ones inside this object
     *
     * @return May return null in case the request param was null
     */
    public Vector2 cloneByValue() {
        Vector2 newVect = new Vector2();

        newVect.dimen(fWidth, fHeight);
        newVect.coord(fX, fY);
        newVect.position(nTop, nBottom);

        return newVect;
    }
}