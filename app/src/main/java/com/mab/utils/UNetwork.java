package com.mab.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.apache.http.conn.util.InetAddressUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * Created by T. Lorand on 12/8/2014.
 */
public class UNetwork {

    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            UDebug.printExceptionStackTrace(e);
            return false;
        }
    }

    /**
     * In case the a url redirects to another, use this method to obtain the final url
     *
     * @return
     */
    public static void getFinalUrl(final String url, final IFinalUrlListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                if (!UString.stringExists(url)) {
                    if (listener != null) listener.onFinalUrlError();
                }

                CharSequence uri = url;
                URL url = null;
                try {
                    url = new URL(uri.toString());
                } catch (MalformedURLException e) {
                    if (listener != null) listener.onFinalUrlError();
                    return;
                }
                HttpURLConnection httpURLConnection = null;
                try {
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    if (listener != null) listener.onFinalUrlError();
                    return;
                }
                httpURLConnection.setInstanceFollowRedirects(false);
                try {
                    httpURLConnection.connect();
                } catch (IOException e) {
                    if (listener != null) listener.onFinalUrlError();
                    return;
                }
                int responseCode = 0;
                try {
                    responseCode = httpURLConnection.getResponseCode();
                } catch (IOException e) {
                    if (listener != null) listener.onFinalUrlError();
                    return;
                }
                String header = httpURLConnection.getHeaderField("Location");

                if (listener != null) {
                    listener.onFinalUrlObtained(header);
                }

            }
        }).start();
    }

    /**
     * @param useIPv4 boolean - true returns IPv4 otherwise IPv6
     * @return the device IP address
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    //------------------------------------------------------ INTERFACE(start)
    public static interface IFinalUrlListener {
        /**
         * NOTE !! This will return on a non-UI thread
         *
         * @param finalUrl
         */
        void onFinalUrlObtained(String finalUrl);

        /**
         * NOTE !! This will return on a non-UI thread
         */
        void onFinalUrlError();
    }
    //------------------------------------------------------ INTERFACE(end)
}