package com.mab.callbacks;

import android.view.MotionEvent;
import android.view.View;

/**
 * A custom touch listener which offers the additional information  of the position of the item which is currently being touched and some additional information linked to it<br/>
 *
 * <b>How to use this:<b/><br/>
 * Attach an onTouchListener to the list item in the getView method of the adapter like so:
 * view.setOnTouchListener(new ListItemTouchCallback<myItemType>(mTouchCallback, myItem, position));
 *
 * @author MAB
 */
public class ListItemTouchCallback<T> implements View.OnTouchListener {

    /**
     * The listener which is passed to us and we'll need to call when we detect an event
     */
    private OnListItemTouchListener mListener;

    /**
     * An object containing information related to the clicked item
     */
    private T mItem;

    /**
     * The position of the item which is currently clicked
     */
    private int mPosition;

    /**
     * @param listener
     * @param item     the object containing some information related to the clicked list item
     * @param position the position of the item in the list
     */
    public ListItemTouchCallback(OnListItemTouchListener listener, T item, int position) {
        this.mListener = listener;
        this.mItem = item;
        this.mPosition = position;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (mListener != null) {
            return mListener.onListItemTouch(view, motionEvent, mItem, mPosition);
        }

        return false;
    }

    //------------------------------------- INTERFACE (start)

    /**
     * Same as {@link android.view.View.OnTouchListener} but with extended information capabilities
     */
    public interface OnListItemTouchListener<T> {
        public boolean onListItemTouch(View view, MotionEvent motionEvent, T item, int position);
    }
    //------------------------------------- INTERFACE (end)

}