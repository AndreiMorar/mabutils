package com.mab.callbacks;

import android.view.View;

/**
 * A custom click listener which offers the additional information  of the position of the item which is currently being clicked and some additional information linked to it<br/>
 *
 * <b>How to use this:<b/><br/>
 * Attach an onClickListener to the list item in the getView method of the adapter like so:
 * view.setOnClickListener(new ListItemClickCallback<myItemType>(mClickCallback, myItem, position));
 *
 * @author MAB
 */
public class ListItemClickCallback<T> implements View.OnClickListener {
    /**
     * The listener which is passed to us and we'll need to call when we detect an event
     */
    private OnListItemClickListener mListener;

    /**
     * An object containing information related to the clicked item
     */
    private T mItem;

    /**
     * The position of the item which is currently clicked
     */
    private int mPosition;

    /**
     * @param listener
     * @param item     the object containing some information related to the clicked list item
     * @param position the position of the item in the list
     */
    public ListItemClickCallback(OnListItemClickListener listener, T item, int position) {
        this.mListener = listener;
        this.mItem = item;
        this.mPosition = position;
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onListItemClicked(view, mItem, mPosition);
        }
    }
    //------------------------------------- INTERFACE (start)

    /**
     * Same as {@link android.view.View.OnClickListener} but with extended information capabilities
     */
    public interface OnListItemClickListener<T> {
        public boolean onListItemClicked(View view, T item, int position);
    }
    //------------------------------------- INTERFACE (end)
}