package com.mab.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * @author MAB
 */
public class SquareRelativeLayoutH extends RelativeLayout {

    public SquareRelativeLayoutH(Context context) {
        super(context);
    }

    public SquareRelativeLayoutH(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareRelativeLayoutH(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    }
}