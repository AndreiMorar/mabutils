package com.mab.ui.customviews;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * A viewpager that has the additional functionality that it can be frozen, i.e. no more left/right swipe
 *
 * @author MAB
 */

public class FreezableViewPager extends ViewPager {

    private boolean isPagingEnabled = true;

    public FreezableViewPager(Context context) {
        super(context);
    }

    public FreezableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }

    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }
}