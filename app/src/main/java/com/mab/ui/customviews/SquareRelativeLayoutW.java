package com.mab.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * @author MAB
 */
public class SquareRelativeLayoutW extends RelativeLayout {

    public SquareRelativeLayoutW(Context context) {
        super(context);
    }

    public SquareRelativeLayoutW(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareRelativeLayoutW(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}