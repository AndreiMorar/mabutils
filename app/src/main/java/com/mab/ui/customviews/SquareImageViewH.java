package com.mab.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class SquareImageViewH extends ImageView {
    public SquareImageViewH(Context context) {
        super(context);
    }

    public SquareImageViewH(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageViewH(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    }
}