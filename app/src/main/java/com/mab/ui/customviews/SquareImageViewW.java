package com.mab.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class SquareImageViewW extends ImageView {
    public SquareImageViewW(Context context) {
        super(context);
    }

    public SquareImageViewW(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageViewW(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}